/*********************************************
*   George Koskeridis (c) 2015, see LICENSE
*       nev_player.c
 *********************************/

#include "nev.h"

#define TEST_MAX_ACCEL 1

#define RESET_SPEED_X 0
#define RESET_SPEED_Y 0


/*
 * Creates a new nev_Player object
 */
nev_PlayerPtr nev_newPlayer(float start_x, float start_y)
{
   if (debugging_is_on) {
      nev_printdbg("");
      nev_printdbg("\tnev_newPlayer");
   }
   if (!nev_display) return NULL;

   nev_PlayerPtr nev_new = (nev_PlayerPtr)al_malloc(sizeof(nev_Player));
   if (!nev_new)
      nev_abort_game("malloc failed for nev_newPlayer");

   nev_new->body_h = (window_height > window_width)?window_height/15:window_width/15;
   nev_new->body_w = (window_height > window_width)?window_width/11:window_height/11;
   nev_new->eye_h = window_height/60;
   nev_new->eye_w = window_height/60;
   nev_new->head_h = window_height/18;
   nev_new->head_w = window_height/17;

   nev_new->body = al_create_bitmap(nev_new->body_w, nev_new->body_h);
   if (!nev_new->body) {
      al_free(nev_new);
      nev_abort_game("Failed creating nev_Player body");
   }
   al_set_target_bitmap(nev_new->body);
   al_clear_to_color(al_map_rgb(10, 42, 42));

   nev_new->head = al_create_bitmap(nev_new->head_w, nev_new->head_h);
   if (!nev_new->head) {
      al_free(nev_new->body);
      al_free(nev_new);
      nev_abort_game("Failed creating nev_Player head");
   }
   al_set_target_bitmap(nev_new->head);
   al_clear_to_color(al_map_rgb(60, 50, 50));

   nev_new->eye = al_create_bitmap(nev_new->eye_w, nev_new->eye_h);
   if (!nev_new->eye) {
      al_free(nev_new->head);
      al_free(nev_new->body);
      al_free(nev_new);
      nev_abort_game("Failed creating nev_Player eye");
   }
   al_set_target_bitmap(nev_new->eye);
   al_clear_to_color(al_map_rgb(255, 255, 255));

   nev_new->pltfrm_idx = -1;

   nev_new->lefteye_den.x   = 0.007;
   nev_new->righteye_den.x  = 0.038;
   nev_new->lefteye_den.y   = 0.016;
   nev_new->righteye_den.y  = 0.016;

   nev_new->leftbndry   = 9*nev_new->body_w;
   nev_new->rightbndry  = window_width - 9*nev_new->body_w;
   nev_new->upbndry     = nev_new->body_h;
   nev_new->downbndry   = window_height - 2*nev_new->body_h;

   /* Set initial speed */
   nev_new->d.x = 0;
   nev_new->d.y = 0;
   nev_new->max_velocity.x = 6;
   nev_new->max_velocity.y = 8;

   /* Set initial acceleration */
   nev_new->acc.x = TEST_MAX_ACCEL;
   nev_new->acc.y = 1.5;

   nev_new->jumps = 0;
   nev_new->moving = nev_new->jumping = false;
   (start_x >= nev_new->rightbndry && (start_x <= nev_new->leftbndry))?(nev_new->body_pos.x = start_x):(nev_new->body_pos.x = window_width/2);
   (start_y >= 0 && (start_y <= window_width - nev_new->body_w))?(nev_new->body_pos.y = start_y):(nev_new->body_pos.y = window_height/2);
   nev_resetPlayer(nev_new);
   al_set_target_backbuffer(nev_display);

   if (debugging_is_on) nev_printdbg("nev_newPlayer() success");
   return nev_new;
}

/*
 * Frees the resources allocated by a nev_newPlayer() call from a nev_Player object
 */
void nev_destroyPlayer(nev_PlayerPtr curr_nevPlayer)
{
   if (curr_nevPlayer) {
      al_destroy_bitmap(curr_nevPlayer->body);
      al_destroy_bitmap(curr_nevPlayer->head);
      al_destroy_bitmap(curr_nevPlayer->eye);
      al_free(curr_nevPlayer);
      curr_nevPlayer = NULL;
      if (debugging_is_on) nev_printdbg("nev_Player is freed");
   }
}

/*
 * Moves a nev_Player object's eyes to the direction
 * specified by the flag parameter
 */
void nev_movePlayerEyes(nev_PlayerPtr curr_nevPlayer, const int flag)
{
   if (curr_nevPlayer) {
      switch (flag) {
      case KEY_LEFT:
         curr_nevPlayer->lefteye_den.x = 0.001;
         curr_nevPlayer->righteye_den.x = 0.025;
         break;
      case KEY_RIGHT:
         curr_nevPlayer->lefteye_den.x = 0.017;
         curr_nevPlayer->righteye_den.x = 0.044;
         break;
      case KEY_DOWN:
         curr_nevPlayer->lefteye_den.y = 0.03;
         curr_nevPlayer->righteye_den.y = 0.03;
         break;
      case KEY_UP:
         curr_nevPlayer->lefteye_den.y = 0.002;
         curr_nevPlayer->righteye_den.y = 0.002;
         break;
      case RESET:
         curr_nevPlayer->lefteye_den.x = 0.007;
         curr_nevPlayer->righteye_den.x = 0.038;
         curr_nevPlayer->lefteye_den.y = 0.016;
         curr_nevPlayer->righteye_den.y = 0.016;
         break;
      default:
         if (debugging_is_on)
            nev_printfdbg("Unrecognized %d flag on nev_movePlayerEyes() call", flag);
         break;
      }
      nev_resetPlayer(curr_nevPlayer);
   }
}

/*
 * Changes the values of a nev_Player object to correlate with the
 * changed body_pos and eye_den values
 */
void nev_resetPlayer(nev_PlayerPtr curr_nevPlayer)
{
   if (curr_nevPlayer) {
      curr_nevPlayer->head_pos.x = curr_nevPlayer->body_pos.x + window_width*0.0083;
      curr_nevPlayer->head_pos.y = curr_nevPlayer->body_pos.y - curr_nevPlayer->head_h;
      curr_nevPlayer->lefteye_pos.x = curr_nevPlayer->head_pos.x + window_height*curr_nevPlayer->lefteye_den.x;
      curr_nevPlayer->lefteye_pos.y = curr_nevPlayer->head_pos.y + window_height*curr_nevPlayer->lefteye_den.y;
      curr_nevPlayer->righteye_pos.x = curr_nevPlayer->head_pos.x + window_height*curr_nevPlayer->righteye_den.x;
      curr_nevPlayer->righteye_pos.y = curr_nevPlayer->head_pos.y + window_height*curr_nevPlayer->righteye_den.y;
   }
}

/*
 * Drawing of a nev_Player and his body parts' happens here
 */
void nev_drawPlayer(nev_PlayerPtr curr_nevPlayer)
{
   if (nev_display && curr_nevPlayer) {
      al_draw_bitmap(curr_nevPlayer->body, curr_nevPlayer->body_pos.x, curr_nevPlayer->body_pos.y, 0);
      al_draw_bitmap(curr_nevPlayer->head, curr_nevPlayer->head_pos.x, curr_nevPlayer->head_pos.y, 0);
      al_draw_bitmap(curr_nevPlayer->eye, curr_nevPlayer->lefteye_pos.x, curr_nevPlayer->lefteye_pos.y, 0);
      al_draw_bitmap(curr_nevPlayer->eye, curr_nevPlayer->righteye_pos.x, curr_nevPlayer->righteye_pos.y, 0);
   }
}

/*
 * Change the velocity of a nev_Player object during the game
 */
void nev_setPlayerSpeed(nev_PlayerPtr curr_nevPlayer, const int flag)
{
   if (curr_nevPlayer) {
      switch (flag) {
      case KEY_UP:
         if (curr_nevPlayer->d.y > -curr_nevPlayer->max_velocity.y) {
            curr_nevPlayer->d.y -= curr_nevPlayer->acc.y;
            curr_nevPlayer->moving = true;
         }
         break;
      case KEY_DOWN:
         if (curr_nevPlayer->d.y < curr_nevPlayer->max_velocity.y) {
            curr_nevPlayer->d.y += curr_nevPlayer->acc.y;
            curr_nevPlayer->moving = true;
         }
         break;
      case KEY_LEFT:
         if (curr_nevPlayer->d.x > -curr_nevPlayer->max_velocity.x) {
            curr_nevPlayer->d.x -= curr_nevPlayer->acc.x;
            curr_nevPlayer->moving = true;
         }
         break;
      case KEY_RIGHT:
         if (curr_nevPlayer->d.x < curr_nevPlayer->max_velocity.x) {
            curr_nevPlayer->d.x += curr_nevPlayer->acc.x;
            curr_nevPlayer->moving = true;
         }
         break;
      case RESET|KEY_UP:
         if (curr_nevPlayer->d.y)
            curr_nevPlayer->d.y = RESET_SPEED_Y;
         break;
      case RESET|KEY_LEFT:
         if (curr_nevPlayer->d.x)
            curr_nevPlayer->d.x = RESET_SPEED_X;
         break;
      default:
      if (debugging_is_on) nev_printfdbg("Unrecognized %d flag on nev_setPlayerSpeed() call", flag);
         break;
      }
   }
}

/*
 * Change the position of a nev_Player object according
 * to its current velocity
 */
void nev_movePlayer(nev_PlayerPtr curr_nevPlayer, const int flag)
{
   if (curr_nevPlayer) {
   switch (flag) {
      case KEY_UP:
         if (curr_nevPlayer->body_pos.y >= curr_nevPlayer->upbndry || curr_nevPlayer->jumping) {
            curr_nevPlayer->body_pos.y += curr_nevPlayer->d.y;
         }
         break;
      case KEY_DOWN:
         if (curr_nevPlayer->body_pos.y <= curr_nevPlayer->downbndry) {
            curr_nevPlayer->body_pos.y += curr_nevPlayer->d.y;
         }
         break;
      case KEY_LEFT:
         if (curr_nevPlayer->body_pos.x >= curr_nevPlayer->leftbndry) {
            curr_nevPlayer->body_pos.x += curr_nevPlayer->d.x;
         }
         break;
      case KEY_RIGHT:
         if (curr_nevPlayer->body_pos.x <= curr_nevPlayer->rightbndry) {
            curr_nevPlayer->body_pos.x += curr_nevPlayer->d.x;
         }
         break;
      case RESET:
         break;
      default:
         if (debugging_is_on) nev_printfdbg("Unrecognized %d flag on nev_movePlayer() call", flag);
         break;
      }
      nev_resetPlayer(curr_nevPlayer);
   }
}

/*********************************************
*   George Koskeridis (c) 2015, see LICENSE
*       nev-error.c
 *********************************/

#include "nev.h"
#include <limits.h>

static unsigned long debugging_line = 1;

extern void nev_Shutdown(void);


/*
 * Standard failure function that aborts the game, does cleanup,
 * shows an error message box with the 'msg' error message
 * and prints 'msg' to stderr
 */
void nev_abort_game(char *msg)
{
   if (debugging_is_on) {
      nev_printdbg("");
      nev_printdbg("\tnev_abort_game");
   }

   if (msg) {
      nev_Shutdown();
      al_show_native_message_box(NULL, "Aborting game!", "Something happened!",
               msg, NULL, ALLEGRO_MESSAGEBOX_ERROR);
      nev_printdbg(msg);
      exit(EXIT_FAILURE);
   } else if (debugging_is_on) {
      nev_printdbg("nev_abort_game failed!");
   }
}

/*
 * Dbg: Sends the error message string 'msg' to stderr
 */
inline void nev_printdbg(char *msg)
{
   debugging_line = (debugging_line == ULONG_MAX)?1:debugging_line;
   fprintf(stderr, "%4ld ==  %s\n", debugging_line++, msg);
}

/*
 * Dbg: Same function but also accepts variadic arguments
 */
inline void nev_printfdbg(char *msg, ...)
{
   debugging_line = (debugging_line == ULONG_MAX)?1:debugging_line;
   fprintf(stderr, "%4ld ==  ", debugging_line++);
   va_list arg;
   va_start(arg, msg);
   vfprintf(stderr, msg, arg);
   va_end(arg);
   fprintf(stderr, "\n");
}

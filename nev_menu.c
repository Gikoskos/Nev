/*********************************************
*   George Koskeridis (c) 2015, see LICENSE
*       nev_menu.c
 *********************************/

#include "nev.h"

#define TOTAL_MENU 3 /*total choices in the main menu*/
#define TOTAL_OPTIONS 3 /*total choices in the options menu*/

enum menu_options {
   MAIN_MENU_START, MAIN_MENU_OPTIONS, MAIN_MENU_EXIT,
   OPTIONS_MENU_RESOLUTION, OPTIONS_MENU_DEBUGGING,
   OPTIONS_MENU_BACK
};



/*Globals*/
/*static*/
char *menu_titles[] = {"Start game", "Options", "Exit"};
char *options_titles[] = {"Resolution", "Debugging", "<"};
char *resolutions[] = {"1000x400", "1200x480", "1500x600", "1800x720"};
bool is_options_menu = false;
bool options_to_menu_anim = false;
bool menu_to_options_anim = false;
enum menu_options menu_choice;
struct coords mouse_pos;
/*non-static*/
/*extern*/
extern bool nev_keyboard[TOTAL_KEYS];
extern ALLEGRO_FONT *DroidSansMono_12;
extern ALLEGRO_FONT *lunchds_small;
extern ALLEGRO_FONT *lunchds_big;
extern bool game_running, menu_running, nev_running;
extern ALLEGRO_EVENT_QUEUE *event_queue;
extern ALLEGRO_TIMER *timer;
extern bool debugging_is_on;

/* Function prototypes */
void drawMenuTitlesAnimLeft(char **s, float start_x, float start_y, float max, float step);
void drawMenuTitlesAnimRight(char **s, float start_x, float start_y, float max, float step);


/*
 * Keyboard message handler function for the main menu messages
 */
inline void menu_keyboard_events(ALLEGRO_EVENT ev, bool *redraw, int *menu_idx)
{
   switch (ev.type) {
   case ALLEGRO_EVENT_TIMER:
      if (nev_keyboard[KEY_UP] && !nev_keyboard[KEY_DOWN]) {
         if (menu_choice != OPTIONS_MENU_RESOLUTION) {
            menu_choice = -1;
            nev_keyboard[KEY_UP] = false;
            if (!is_options_menu) {
               if (!(*menu_idx)) *menu_idx = TOTAL_MENU - 1;
               else (*menu_idx)--;
            } else {
               if (!(*menu_idx)) *menu_idx = TOTAL_OPTIONS - 1;
               else (*menu_idx)--;
            }
         }
      }
      if (nev_keyboard[KEY_DOWN] && !nev_keyboard[KEY_UP]) {
         if (menu_choice != OPTIONS_MENU_RESOLUTION) {
            menu_choice = -1;
            nev_keyboard[KEY_DOWN] = false;
            if (!is_options_menu) {
               if ((*menu_idx) == TOTAL_MENU - 1) *menu_idx = 0;
               else (*menu_idx)++;
            } else {
               if ((*menu_idx) == TOTAL_OPTIONS - 1) *menu_idx = 0;
               else (*menu_idx)++;
            }
         }
      }
      if (nev_keyboard[KEY_LEFT] && !nev_keyboard[KEY_RIGHT]) {
         if (is_options_menu) {
            if (menu_choice == OPTIONS_MENU_RESOLUTION) {
               if (!(*menu_idx)) *menu_idx = 3;
               else (*menu_idx)--;
            }
         }
      }
      if (nev_keyboard[KEY_RIGHT] && !nev_keyboard[KEY_LEFT]) {
         if (is_options_menu) {
            if (menu_choice == OPTIONS_MENU_RESOLUTION) {
               if ((*menu_idx) == 3) *menu_idx = 0;
               else (*menu_idx)--;
            }
         }
      }
      *redraw = true;
      break;
   case ALLEGRO_EVENT_MOUSE_AXES:
   case ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY:
      mouse_pos.x = ev.mouse.x;
      mouse_pos.y = ev.mouse.y;
      break;
   case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
      if (!is_options_menu) {
         menu_choice = *menu_idx;
      } else {
         menu_choice = (*menu_idx) + TOTAL_MENU;
         if (menu_choice == OPTIONS_MENU_DEBUGGING) {
            if (debugging_is_on) nev_printdbg("Debugging messages disabled");
            else nev_printdbg("Debugging messages enabled");
            debugging_is_on = !debugging_is_on;
         }
      }
      break;
   case ALLEGRO_EVENT_KEY_UP:
      switch (ev.keyboard.keycode) {
      case ALLEGRO_KEY_UP:
         nev_keyboard[KEY_UP] = true;
         break;
      case ALLEGRO_KEY_DOWN:
         nev_keyboard[KEY_DOWN] = true;
         break;
      case ALLEGRO_KEY_Z:
      case ALLEGRO_KEY_ENTER:
         if (nev_keyboard[KEY_ENTER]) {
            if (!is_options_menu) {
               menu_choice = *menu_idx;
            } else {
               menu_choice = (*menu_idx) + TOTAL_MENU;
               if (menu_choice == OPTIONS_MENU_DEBUGGING) {
                  if (debugging_is_on) nev_printdbg("Debugging messages disabled");
                  else nev_printdbg("Debugging messages enabled");
                  debugging_is_on = !debugging_is_on;
               }
            }
         }
         break;
      case ALLEGRO_KEY_ESCAPE:
         if (!is_options_menu) {
            menu_running = nev_running = false;
         } else {
            if (menu_choice == OPTIONS_MENU_RESOLUTION) {
               menu_choice = -1;
            } else {
               is_options_menu = false;
            }
         }
         break;
      }
      break;
   case ALLEGRO_EVENT_KEY_DOWN:
      switch (ev.keyboard.keycode) {
      case ALLEGRO_KEY_UP:
         nev_keyboard[KEY_UP] = false;
         break;
      case ALLEGRO_KEY_DOWN:
         nev_keyboard[KEY_DOWN] = false;
         break;
      case ALLEGRO_KEY_Z:
      case ALLEGRO_KEY_ENTER:
         nev_keyboard[KEY_ENTER] = true;
         break;
      }
      break;
   case ALLEGRO_EVENT_DISPLAY_CLOSE:
      menu_running = nev_running = false;
      break;
   }
}

/*
 * All the main menu drawing and logic goes here
 */
void main_menu_loop(void)
{
   if (debugging_is_on) nev_printdbg("\tmain_menu_loop");

   al_register_event_source(event_queue, al_get_mouse_event_source());
   ResetBoolArray(nev_keyboard, TOTAL_KEYS);
   bool redraw = false;
   /* the current highlighted choice */
   int curr_menu_idx = 0;

   float menu_y = window_height/10;

   float menu_x = window_width - window_width/3;

   /* no option is chosen by the player before the menu loads,
    * so menu_choice takes a negative symbolic value */
   menu_choice = -1;

   //al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW);
   while (menu_running) {
      ALLEGRO_EVENT event;
      al_wait_for_event(event_queue, &event);

      menu_keyboard_events(event, &redraw, &curr_menu_idx);

      if (redraw && al_is_event_queue_empty(event_queue)) {
         redraw = false;
         if (!is_options_menu) {
            switch (menu_choice) {
            case MAIN_MENU_START:
               game_running = true;
               menu_running = false;
               break;
            case MAIN_MENU_OPTIONS:
               menu_to_options_anim = true;
               curr_menu_idx = 0;
               break;
            case MAIN_MENU_EXIT:
               menu_running = nev_running = false;
               break;
            default:
               break;
            }
         } else {
            switch (menu_choice) {
            case OPTIONS_MENU_RESOLUTION:
               break;
            case OPTIONS_MENU_BACK:
               options_to_menu_anim = true;
               curr_menu_idx = 1;
               break;
            default:
               break;
            }
         }
         al_clear_to_color(NEV_BLACK);
         al_draw_text(lunchds_big, NEV_CYAN, 50, 100, 0, "Nev");
         al_draw_text(DroidSansMono_12, NEV_CYAN, 158, 240, 0, "v"NEV_VERSION_STRING);

         /* if we're not in the options menu there will either be drawn
          * main menu to options menu transition animation, or just the main menu */
         if (!is_options_menu) {
            /* draw the main menu to options menu transition animation */
            if (menu_to_options_anim) {
               drawMenuTitlesAnimRight(menu_titles, menu_x, 240, window_width, 20);
               drawMenuTitlesAnimLeft(options_titles, window_width, 240, menu_x, -20);
               is_options_menu = true;
               menu_to_options_anim = false;
            /* draw the main menu */
            } else {
               int i, j;
               for (i = MAIN_MENU_START, j = 240; i < TOTAL_MENU; i++, j+=menu_y) {
                  if ((mouse_pos.x >= menu_x && mouse_pos.x <= menu_x + 240) &&
                       (mouse_pos.y >= j && mouse_pos.y <= j - 7 + menu_y)) {
                     curr_menu_idx = i;
                     al_draw_text(lunchds_small, NEV_STEELBLUE, menu_x, j, 0, menu_titles[i]);
                  } else if (curr_menu_idx == i) {
                     al_draw_text(lunchds_small, NEV_STEELBLUE, menu_x, j, 0, menu_titles[i]);
                  } else {
                     al_draw_text(lunchds_small, NEV_WHITE, menu_x, j, 0, menu_titles[i]);
                  }
               }
            }
         } else {
            /* draw the transition animation from the options menu to the main menu */
            if (options_to_menu_anim) {
               drawMenuTitlesAnimRight(options_titles, menu_x, 240, window_width, 20);
               drawMenuTitlesAnimLeft(menu_titles, window_width, 240, menu_x, -20);
               options_to_menu_anim = false;
               is_options_menu = false;
            /* draw the options menu */
            } else {
               int i, j;
               for (i = 0, j = 240; i < TOTAL_OPTIONS; i++, j+=menu_y) {
                  if (i == 0) {
                     if (menu_choice == OPTIONS_MENU_RESOLUTION) {
                        al_draw_text(lunchds_small, NEV_RED, menu_x, j, 0, options_titles[i]);
                        continue;
                     }
                  }
                  if ((mouse_pos.x >= menu_x && mouse_pos.x <= menu_x + 240) &&
                       (mouse_pos.y >= j && mouse_pos.y <= j - 7 + menu_y)) {
                     curr_menu_idx = i;
                     al_draw_text(lunchds_small, NEV_STEELBLUE, menu_x, j, 0, options_titles[i]);
                  } else if (curr_menu_idx == i) {
                     al_draw_text(lunchds_small, NEV_STEELBLUE, menu_x, j, 0, options_titles[i]);
                  } else {
                     al_draw_text(lunchds_small, NEV_WHITE, menu_x, j, 0, options_titles[i]);
                  }
               }
            }
         }
         al_flip_display();
      }
   }
   al_flush_event_queue(event_queue);
}

void drawMenuTitlesAnimLeft(char **s, float start_x, float start_y, float max, float step)
{
   int i, j;
   for (i = start_x, j = start_y; i >= max; i += step) {
      al_clear_to_color(NEV_BLACK);
      al_hold_bitmap_drawing(true);
      al_draw_text(lunchds_big, NEV_CYAN, 50, 100, 0, "Nev");
      al_draw_text(DroidSansMono_12, NEV_CYAN, 158, 240, 0, "v"NEV_VERSION_STRING);
      al_draw_text(lunchds_small, NEV_WHITE, i, j, 0, s[0]);
      al_draw_text(lunchds_small, NEV_WHITE, i, j + window_height/10, 0, s[1]);
      al_draw_text(lunchds_small, NEV_WHITE, i, j + 2*(window_height/10), 0, s[2]);
      al_hold_bitmap_drawing(false);
      al_flip_display();
   }
}

void drawMenuTitlesAnimRight(char **s, float start_x, float start_y, float max, float step)
{
   int i, j;
   for (i = start_x, j = start_y; i < max; i += step) {
      al_clear_to_color(NEV_BLACK);
      al_hold_bitmap_drawing(true);
      al_draw_text(lunchds_big, NEV_CYAN, 50, 100, 0, "Nev");
      al_draw_text(DroidSansMono_12, NEV_CYAN, 158, 240, 0, "v"NEV_VERSION_STRING);
      al_draw_text(lunchds_small, NEV_WHITE, i, j, 0, s[0]);
      al_draw_text(lunchds_small, NEV_WHITE, i, j + window_height/10, 0, s[1]);
      al_draw_text(lunchds_small, NEV_WHITE, i, j + 2*(window_height/10), 0, s[2]);
      al_hold_bitmap_drawing(false);
      al_flip_display();
   }
}

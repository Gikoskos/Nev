/*********************************************
*   George Koskeridis (c) 2015, see LICENSE
*       nev_stage.c
 *********************************/

#include "nev.h"

#define TEST_BNDRY 25

/* limits for the minimum and maximum platform number generated */
#define MAX_PLTFRM 16
#define MIN_PLTFRM 10



/*
 * Create a new nev_newBckrnd object from the path of an image.
 * The flag determines the number of the platforms created.
 * A RANDOM_PLATFORMS flag creates randomized platforms.
 */
nev_BckrndPtr nev_newBackground(const char *bckrnd_image_path, const int flag)
{
   /* max height of the platforms */
   int max_platform_height = window_height/20;

   if (debugging_is_on) {
      nev_printdbg("");
      nev_printdbg("\tnev_newBackground");
   }

   if (!nev_display || !bckrnd_image_path) return NULL;

   nev_BckrndPtr nev_new = (nev_BckrndPtr)al_malloc(sizeof(nev_Bckrnd));
   if (!nev_new)
      nev_abort_game("malloc failed for nev_newBackground");

   nev_new->bckrnd_bmp = al_load_bitmap(bckrnd_image_path);
   if (!nev_new->bckrnd_bmp) {
      al_free(nev_new);
      nev_abort_game("Failed to load " str(bckrnd_image_path));
   }
   nev_new->viewport.x = (window_width - al_get_bitmap_width(nev_new->bckrnd_bmp))/2;
   nev_new->viewport.y = (window_height - al_get_bitmap_height(nev_new->bckrnd_bmp))/2;
   if (debugging_is_on) nev_printfdbg("New stage viewport x:"FRMT_FLOAT" y:"FRMT_FLOAT,
                                      nev_new->viewport.x, nev_new->viewport.y);

   if (flag == RANDOM_PLATFORMS) {
      if (debugging_is_on) nev_printdbg("Initializing randomized platforms");
      srand(time(NULL));
      nev_new->total_platforms = _getRangedRand(MIN_PLTFRM, MAX_PLTFRM);
   } else if (flag >= 0) {
      if (debugging_is_on) nev_printfdbg("Initializing %d platforms", flag);
      nev_new->total_platforms = flag;
   } else {
      if (debugging_is_on) nev_printfdbg("Initializing 16 static platforms", flag);
      nev_new->total_platforms = 16;
   }

   nev_new->platforms = (nev_PltfrmPtr*)al_malloc(sizeof(nev_Pltfrm)*nev_new->total_platforms);
   if (!nev_new->platforms)
      nev_abort_game("Insufficient memory to allocate platforms");

   int i;
   for (i = 0; i < nev_new->total_platforms; i++) {
      nev_new->platforms[i] = (nev_PltfrmPtr)al_malloc(sizeof(nev_Pltfrm));
      if (!nev_new->platforms[i]) {
         nev_abort_game("Insufficient memory to allocate platform #");
      }

      nev_new->platforms[i]->height = max_platform_height;
      if (flag == RANDOM_PLATFORMS) {
         nev_new->platforms[i]->width = (unsigned int)_getRangedRand(200, 500);
      } else {
         nev_new->platforms[i]->width = 50;
      }

      nev_new->platforms[i]->platform_bmp = al_create_bitmap(nev_new->platforms[i]->width,
                                                             nev_new->platforms[i]->height);
      al_set_target_bitmap(nev_new->platforms[i]->platform_bmp);
      if (!nev_new->platforms[i]->platform_bmp)
         nev_abort_game("Platform creation failure");

      al_clear_to_color(nev_rand_rgb());
      nev_new->platforms[i]->pltf_gravity = false;
      if (flag == RANDOM_PLATFORMS) {
         nev_new->platforms[i]->pltf_upper.x = (float)_getRangedRand(-window_width + 500,
                                                                     window_width - 500);
         nev_new->platforms[i]->pltf_upper.y = (float)_getRangedRand(-window_height + 2*max_platform_height,
                                                                     window_height - 2*max_platform_height);
      } else {
         if (!i) {
            nev_new->platforms[i]->pltf_upper.x = (float)al_get_bitmap_width(nev_new->bckrnd_bmp)/15;
            nev_new->platforms[i]->pltf_upper.y = (float)al_get_bitmap_height(nev_new->bckrnd_bmp)/15;
         } else {
            nev_new->platforms[i]->pltf_upper.x = nev_new->platforms[i-1]->pltf_upper.x +
                                                      1.5*nev_new->platforms[i-1]->width;

            nev_new->platforms[i]->pltf_upper.y = nev_new->platforms[i-1]->pltf_upper.y +
                                                      1.5*nev_new->platforms[i-1]->height;
         }
      }
      if (debugging_is_on) nev_printfdbg("Platform %d with width = %u and height = %u"
                                         " initialized on position (x,y) = ("FRMT_FLOAT
                                         ", "FRMT_FLOAT")", i, nev_new->platforms[i]->width,
                                         nev_new->platforms[i]->height, nev_new->platforms[i]->pltf_upper.x,
                                         nev_new->platforms[i]->pltf_upper.y);
   }

   if (debugging_is_on) nev_printfdbg("Total platforms initialized: %u", nev_new->total_platforms);
   al_set_target_backbuffer(nev_display);

   /*initially the stage doesn't have gravity; to enable it call nev_enableGravity()*/
   nev_new->gravity = false;

   if (debugging_is_on) nev_printdbg("nev_newBackground() success");
   return nev_new;
}

/*
 * Frees the resources allocated by a nev_newBackground() call
 */
void nev_destroyBackground(nev_BckrndPtr curr_nevBckrnd)
{
   if (curr_nevBckrnd) {
      int i;
      for (i = 0; i < curr_nevBckrnd->total_platforms; i++) {
         al_destroy_bitmap(curr_nevBckrnd->platforms[i]->platform_bmp);
         al_free((void*)curr_nevBckrnd->platforms[i]);
      }
      al_destroy_bitmap(curr_nevBckrnd->bckrnd_bmp);
      al_free((void**)curr_nevBckrnd->platforms);
      al_free((void*)curr_nevBckrnd);
      curr_nevBckrnd = NULL;
      if (debugging_is_on) nev_printdbg("nev_Background is freed");
   }
}

/*
 * Draws the background and platforms of a nev_Bckrnd object to the main window
 */
void nev_drawBackground(nev_BckrndPtr curr_nevBckrnd)
{
   if (curr_nevBckrnd && nev_display) {
      al_draw_bitmap(curr_nevBckrnd->bckrnd_bmp, curr_nevBckrnd->viewport.x, curr_nevBckrnd->viewport.y, 0);
      int i = 0;
      for (i = 0; i < curr_nevBckrnd->total_platforms; i++) {
         al_draw_bitmap(curr_nevBckrnd->platforms[i]->platform_bmp,
                        curr_nevBckrnd->platforms[i]->pltf_upper.x, curr_nevBckrnd->platforms[i]->pltf_upper.y, 0);
      }
   }
}

/*
 * Moves a nev_Bckrnd object to the same direction of a nev_Player object
 * and sets the object's boundaries on the screen
 */
void nev_updateBackground(nev_BckrndPtr curr_nevBckrnd, nev_PlayerPtr curr_nevPlayer, int flag)
{
   if (curr_nevBckrnd && curr_nevPlayer) {
      curr_nevPlayer->upbndry = curr_nevPlayer->head_pos.y - curr_nevPlayer->head_h;
      if (curr_nevBckrnd->viewport.y >= 0)
         curr_nevPlayer->upbndry = 2*curr_nevPlayer->head_h;

      curr_nevPlayer->downbndry = window_height - 2*curr_nevPlayer->body_h;
      if (curr_nevBckrnd->viewport.y <= window_height - al_get_bitmap_height(curr_nevBckrnd->bckrnd_bmp))
         curr_nevPlayer->downbndry = window_height - (curr_nevPlayer->body_h + curr_nevPlayer->head_h);

      curr_nevPlayer->leftbndry = 9*curr_nevPlayer->body_w;
      if (curr_nevBckrnd->viewport.x >= 0)
         curr_nevPlayer->leftbndry = 2*curr_nevPlayer->body_w;

      curr_nevPlayer->rightbndry = window_width - 9*curr_nevPlayer->body_w;
      if (curr_nevBckrnd->viewport.x <= window_width - al_get_bitmap_width(curr_nevBckrnd->bckrnd_bmp))
         curr_nevPlayer->rightbndry = window_width - 3*curr_nevPlayer->body_w;
      switch (flag) {
      case KEY_UP:
         if (curr_nevBckrnd->viewport.y < 0) {
            if ((curr_nevPlayer->body_pos.y >= curr_nevPlayer->upbndry - TEST_BNDRY &&
                curr_nevPlayer->body_pos.y <= curr_nevPlayer->upbndry + TEST_BNDRY) ||
                curr_nevPlayer->jumping) {
               curr_nevBckrnd->viewport.y -= curr_nevPlayer->d.y/2;
               int i = 0;
               for (i = 0; i < curr_nevBckrnd->total_platforms; i++) {
                  curr_nevBckrnd->platforms[i]->pltf_upper.y -= curr_nevPlayer->d.y;
               }
            }
         }
         break;
      case KEY_DOWN:
         if (curr_nevBckrnd->viewport.y > window_height - al_get_bitmap_height(curr_nevBckrnd->bckrnd_bmp)) {
            if (curr_nevPlayer->body_pos.y >= curr_nevPlayer->downbndry - TEST_BNDRY &&
                curr_nevPlayer->body_pos.y <= curr_nevPlayer->downbndry + TEST_BNDRY) {
               curr_nevBckrnd->viewport.y -= curr_nevPlayer->d.y/2;
               int i = 0;
               for (i = 0; i < curr_nevBckrnd->total_platforms; i++) {
                  curr_nevBckrnd->platforms[i]->pltf_upper.y -= curr_nevPlayer->d.y;
               }
            }
         }
         break;
      case KEY_LEFT:
         if (curr_nevBckrnd->viewport.x < 0) {
            if (curr_nevPlayer->body_pos.x >= curr_nevPlayer->leftbndry - TEST_BNDRY &&
                curr_nevPlayer->body_pos.x <= curr_nevPlayer->leftbndry + TEST_BNDRY) {
               curr_nevBckrnd->viewport.x -= curr_nevPlayer->d.x/2;
               int i = 0;
               for (i = 0; i < curr_nevBckrnd->total_platforms; i++) {
                  curr_nevBckrnd->platforms[i]->pltf_upper.x -= curr_nevPlayer->d.x;
               }
            }
         }
         break;
      case KEY_RIGHT:
         if (curr_nevBckrnd->viewport.x > window_width - al_get_bitmap_width(curr_nevBckrnd->bckrnd_bmp)) {
            if (curr_nevPlayer->body_pos.x >= curr_nevPlayer->rightbndry - TEST_BNDRY &&
                curr_nevPlayer->body_pos.x <= curr_nevPlayer->rightbndry + TEST_BNDRY) {
               curr_nevBckrnd->viewport.x -= curr_nevPlayer->d.x/2;
               int i = 0;
               for (i = 0; i < curr_nevBckrnd->total_platforms; i++) {
                  curr_nevBckrnd->platforms[i]->pltf_upper.x -= curr_nevPlayer->d.x;
               }
            }
         }
         break;
      case RESET:
         break;
      default:
         if (debugging_is_on)
            nev_printfdbg("Unrecognized %d flag on nev_updateBackground() call", flag);
         break;
      }
   }
}

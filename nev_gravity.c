/*********************************************
*   George Koskeridis (c) 2015, see LICENSE
*       nev-gravity.c
 *********************************/

#include "nev.h"
#include <stdarg.h>

extern void nev_update(int flag);


/*
 * Enables gravity in the stage and platforms
 * of a nev_Bckrnd object
 */
void nev_enableStageGravity(nev_BckrndPtr curr_nevBckrnd)
{
   if (curr_nevBckrnd) {
      if (!curr_nevBckrnd->gravity) {
         curr_nevBckrnd->gravity = true;
         int i;
         for (i = 0; i < curr_nevBckrnd->total_platforms; i++)
            curr_nevBckrnd->platforms[i]->pltf_gravity = true;
         if (debugging_is_on) nev_printdbg("Stage gravity enabled");
      }
   }
}

/*
 * Checks if a nev_Player object collides with any platforms
 * in a nev_Bckrnd stage and sets values accordingly
 */
void nev_setPlatformCollision(nev_PlayerPtr curr_nevPlayer, nev_BckrndPtr curr_nevBckrnd)
{
   if (curr_nevBckrnd && curr_nevPlayer) {
      int i;
      bool drop_player = true;
      float right_player_x = curr_nevPlayer->body_pos.x + curr_nevPlayer->body_w/2;
      float down_player_y = curr_nevPlayer->body_pos.y + curr_nevPlayer->body_h;

      for (i = 0; i < curr_nevBckrnd->total_platforms; i++) {
         if (!curr_nevBckrnd->platforms[i]->pltf_gravity) continue;

         float left_pltfrm_x = curr_nevBckrnd->platforms[i]->pltf_upper.x;
         float right_pltfrm_x = curr_nevBckrnd->platforms[i]->pltf_upper.x + curr_nevBckrnd->platforms[i]->width;
         /* if the player is between 6 pixels from a platform's y position */
         if (curr_nevBckrnd->platforms[i]->pltf_upper.y - 8 <= down_player_y &&
             curr_nevBckrnd->platforms[i]->pltf_upper.y + 8 >= down_player_y) {
            if (left_pltfrm_x <= right_player_x && right_pltfrm_x >= right_player_x) {
               /* if the player wasn't standing in this platform before (floating or in another platform) */
               if (curr_nevPlayer->pltfrm_idx != i) {
                  /* set player to be exactly on top of the platform */
                  curr_nevPlayer->body_pos.y = curr_nevBckrnd->platforms[i]->pltf_upper.y - curr_nevPlayer->body_h;
                  nev_setPlayerSpeed(curr_nevPlayer, RESET|KEY_UP);
                  nev_movePlayerEyes(curr_nevPlayer, RESET);
                  nev_resetPlayer(curr_nevPlayer);
               }
               /* set maximum jump height while the player is on top of the platform */
               curr_nevPlayer->max_jump_height = curr_nevBckrnd->platforms[i]->pltf_upper.y - 2.2*curr_nevPlayer->body_h;
               curr_nevPlayer->pltfrm_idx = i;
               drop_player = false;
               break;
            }
         }
      }
      if (drop_player) {
         curr_nevPlayer->pltfrm_idx = -1;
         nev_update(KEY_DOWN);
      }
   }
}

/*
 * Makes a nev_Player object do a single jump
 */
void nev_playerJump(nev_PlayerPtr curr_nevPlayer, nev_BckrndPtr curr_nevBckrnd)
{
   if (curr_nevPlayer && curr_nevBckrnd->gravity) {
      if (curr_nevPlayer->jumping && curr_nevPlayer->body_pos.y > curr_nevPlayer->max_jump_height) {
         double temp_max_vel = curr_nevPlayer->max_velocity.y;
         curr_nevPlayer->max_velocity.y = 15;
         nev_update(KEY_UP);
         curr_nevPlayer->max_velocity.y = temp_max_vel;
      } else if (curr_nevPlayer->jumping) {
         curr_nevPlayer->jumps++;
         curr_nevPlayer->jumping = false;
      }
   }
}

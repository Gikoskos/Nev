/*********************************************
*   George Koskeridis (c) 2015, see LICENSE
*       nev.h
 *********************************/

#ifndef NEV_H
#define NEV_H

#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

/* utility macros */
#define xstr(x) str(x)
#define str(x) #x

#define ResetBoolArray(x, y)                  \
{                                             \
   int i;                                     \
   for (i = 0; i < y; i++) x[i] = false;      \
}


/* versioning */
#define NEV_MAJOR 0
#define NEV_MINOR 0
#define NEV_PATCH 3
#define NEV_VERSION_STRING xstr(NEV_MAJOR)"."xstr(NEV_MINOR)"."xstr(NEV_PATCH)

#if defined(_WIN32) || defined(WIN32) || defined(_WINDOWS_)
#define FRMT_FLOAT "%.4f"
#else
#define FRMT_FLOAT "%.4lf"
#endif // defined

#define RESET 0xAB34

#define FPS 60

/*
 * Some colors for easy use
 */
#define NEV_WHITE al_map_rgb(255, 255, 255)
#define NEV_BLUE al_map_rgb(0, 0, 255)
#define NEV_SKYBLUE al_map_rgb(135, 206, 235)
#define NEV_STEELBLUE al_map_rgb(70, 130, 180)
#define NEV_LIGHTSTEELBLUE al_map_rgb(176, 196, 222)
#define NEV_BLACK al_map_rgb(0, 0, 0)
#define NEV_CYAN al_map_rgb(0, 255, 255)
#define NEV_DRKGREEN al_map_rgb(0, 100, 0)
#define NEV_DRKSEAGREEN al_map_rgb(143, 188, 143)
#define NEV_LAWNGREEN al_map_rgb(124, 252, 0)
#define NEV_OLIVEDRAB al_map_rgb(107, 142, 35)
#define NEV_INDIANRED al_map_rgb(205, 92, 92)
#define NEV_RED al_map_rgb(139, 0, 0)
#define NEV_GRAY al_map_rgb(120, 120, 120)
#define nev_rand_rgb() al_map_rgb((unsigned char)_getRangedRand(0, 255), \
                                  (unsigned char)_getRangedRand(0, 255), \
                                  (unsigned char)_getRangedRand(0, 255))

/*
 * Macros for assets
 */
#define LUNCHDS lunchds.ttf
#define DROIDSANSMONO DroidSansMono.ttf
#define REDSUNSETPALM RedSunsetPalm.jpg
#define APPLEBLACK AppleBlack.jpg
#define NEV_ICON nev.tga
#define NEV_HURT_ICON nev_hurt.tga

#define ICONPATH Assets/Images/Icons/
#define STAGEPATH Assets/Images/Stages/
#define FONTPATH Assets/Fonts/

#define IconPath(x) xstr(ICONPATH) str(x)
#define StagePath(x) xstr(STAGEPATH) str(x)
#define FontPath(x) xstr(FONTPATH) str(x)

/* flag to choose whether platforms created are
 * randomized in a nev_newBackground() call*/
#define RANDOM_PLATFORMS -1

#define TOTAL_KEYS 7 /*total keyboard keys used in the game*/


/*
 * Struct for coordinates
 */
struct coords {
   float x;
   float y;
};

/*
 * Data stored in a nev_Pltfrm platform object
 */
typedef struct {
   /*the coords of the corner
    *of each platform, in pixels*/
   struct coords pltf_upper;

   /*the width and height of the platform*/
	unsigned int width, height;

   /*if true, the platform has gravity*/
   bool pltf_gravity;

	ALLEGRO_BITMAP *platform_bmp;
} nev_Pltfrm, *nev_PltfrmPtr;

/*
 * Data stored in a nev_Bckrnd background object
 */
typedef struct {
   /*the number of platforms on screen*/
   unsigned int total_platforms;

   /*the area of the background image that
    *is viewed on each frame*/
   struct coords viewport;

   /*if true, the stage has gravity*/
   bool gravity;

   nev_PltfrmPtr *platforms;
   ALLEGRO_BITMAP *bckrnd_bmp;
} nev_Bckrnd, *nev_BckrndPtr;

/*
 * Data stored in a nev_Player player object
 */
typedef struct {
   /*is true when the object is in
    *motion, false when it's not */
   bool moving;

   /*is true when the object is
    *jumping and false when it's not*/
   bool jumping;

   /*total number of jumps a player can perform*/
   int jumps;

   /*width and height values of
    *the object's body, head and eyes*/
   float body_w, body_h;
   float head_w, head_h;
   float eye_w, eye_h;

   /*platform number the object is standing on
    *if the object is not on any platform this value is -1*/
   int pltfrm_idx;

   /*this value stores the maximum jump height of the object*/
   float max_jump_height;

   /*maximum value of velocity allowed for both axis*/
   struct coords max_velocity;

   /*velocity vector*/
   struct coords d;

   /*acceleration vector*/
   struct coords acc;

   /*upper left corner coordinates for the body, head and eyes*/
   struct coords body_pos;
   struct coords head_pos;
   struct coords lefteye_pos;
   struct coords righteye_pos;

   /*denominators to change default eye position*/
   struct coords lefteye_den, righteye_den;

   /*screen boundaries that determine the limits of the movement
    *of a 'nev_Player' object in a 'nev_Bckrnd' stage*/
   float rightbndry, leftbndry, upbndry, downbndry;
   ALLEGRO_BITMAP *head;
   ALLEGRO_BITMAP *body;
   ALLEGRO_BITMAP *eye;
   //ALLEGRO_BITMAP *leg, *arm;
} nev_Player, *nev_PlayerPtr;

/*
 * Enum for keyboard keys used in the game
 */
enum keyboard_keys {
   KEY_UP, KEY_LEFT, KEY_RIGHT, KEY_DOWN,
   KEY_Z, KEY_X, KEY_ENTER
};


/*
 * External globals
 */
ALLEGRO_DISPLAY *nev_display;

/* dimensions for the window that is created
 * best looking resolutions multiples of 400x1000 and up */
int window_height;
int window_width;

/* when this value is true, debugging messages appear on the console */
bool debugging_is_on;

/* null-terminated string for the window title */
char *window_title;


/*
 * External functions
 */
/*nev.c*/
unsigned int _getRandomU(const unsigned int in);
int _getRangedRand(const int range_min, const int range_max);

/*nev_menu.c*/
void main_menu_loop(void);
inline void menu_keyboard_events(ALLEGRO_EVENT ev,
                                 bool *redraw,
                                 int *menu_idx) __attribute__((always_inline));

/*nev_error.c*/
void nev_abort_game(char *msg);
extern inline void nev_printdbg(char *msg);
extern inline void nev_printfdbg(char *msg, ...);

/*nev_gravity.c*/
void nev_enableStageGravity(nev_BckrndPtr curr_nevBckrnd);
void nev_setPlatformCollision(nev_PlayerPtr curr_nevPlayer, nev_BckrndPtr curr_nevBckrnd);
void nev_playerJump(nev_PlayerPtr curr_nevPlayer, nev_BckrndPtr curr_nevBckrnd);

/*nev_stage.c*/
nev_BckrndPtr nev_newBackground(const char *bckrnd_image_path, const int flag);
void nev_drawBackground(nev_BckrndPtr curr_nevBckrnd);
void nev_updateBackground(nev_BckrndPtr curr_nevBckrnd, nev_PlayerPtr curr_nevPlayer, const int flag);
void nev_destroyBackground(nev_BckrndPtr curr_nevBckrnd);

/*nev_player.c*/
nev_PlayerPtr nev_newPlayer(float start_x, float start_y);
void nev_drawPlayer(nev_PlayerPtr curr_nevPlayer);
void nev_setPlayerSpeed(nev_PlayerPtr curr_nevPlayer, const int flag);
void nev_movePlayerEyes(nev_PlayerPtr curr_nevPlayer, const int flag);
void nev_resetPlayer(nev_PlayerPtr curr_nevPlayer);
void nev_movePlayer(nev_PlayerPtr curr_nevPlayer, const int flag);
void nev_destroyPlayer(nev_PlayerPtr curr_nevPlayer);

#endif // NEV_H

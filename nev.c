/*********************************************
*   George Koskeridis (c) 2015, see LICENSE
*       nev.c
 *********************************/

#include "xc.h"
#include "nev.h"
#include <getopt.h>
#include <sys/time.h>

/* helper macro for the debugging messages that are printed onscreen */
#define __DBG_MSG al_draw_textf(DroidSansMono_12, al_map_rgb(_dbg_r, _dbg_g, _dbg_b), 10,


/* Globals */
/*static*/
bool pr_message_z = false, pr_message_x = false;
ALLEGRO_BITMAP *nev_ico;
ALLEGRO_BITMAP *nev_hurt_ico;
XC_STATE *x360controller;
unsigned char _dbg_r = 124, _dbg_g = 252, _dbg_b = 0; /*color for the debugging messages*/
/*non-static*/
bool nev_keyboard[TOTAL_KEYS];
bool game_running, menu_running, nev_running;
char *window_title = "Nev";
ALLEGRO_DISPLAY *nev_display;
nev_PlayerPtr main_player;
nev_BckrndPtr test_stage;
ALLEGRO_FONT *DroidSansMono_12;
ALLEGRO_FONT *lunchds_small;
ALLEGRO_FONT *lunchds_big;
ALLEGRO_EVENT_QUEUE *event_queue;
ALLEGRO_TIMER *timer;
/*extern*/


/* Function prototypes */
void nev_init(void);
void run_game(void);
void nev_update(int flag);
void nev_Shutdown(void);
void game_loop(void);
inline void nev_draw_debugmsg(void) __attribute__((always_inline));

#if 0 /*gcc by default doesn't inline; this is for debugging*/
inline void game_keyboard_events(ALLEGRO_EVENT ev, bool *redraw);
#elif 1 /*this is for opt*/
inline void game_keyboard_events(ALLEGRO_EVENT ev,
                                 bool *redraw) __attribute__((always_inline));
#endif


/*
 * Returns a number between a range of range_min and range_max
 */
int _getRangedRand(const int range_min, const int range_max)
{
   return (int)((double)rand()/(RAND_MAX)*(range_max-range_min) + range_min);
}

/*
 * Returns a random number up to in
 */
unsigned int _getRandomU(const unsigned int in)
{
   struct timeval curr_time;
   gettimeofday(&curr_time, NULL);
   long long int z = (long long int)curr_time.tv_usec;
   unsigned int seed = (unsigned int)z;
   srand(seed);
   return rand()%in;
}

/*
 * Initializations for Allegro
 */
void nev_init(void)
{
   if (debugging_is_on) nev_printdbg("\tnev_init");

   main_player = NULL;
   test_stage = NULL;
   nev_ico = NULL;
   nev_hurt_ico = NULL;
   x360controller = NULL;
   DroidSansMono_12 = NULL;
   nev_display = NULL;
   lunchds_small = NULL;
   lunchds_big = NULL;
   event_queue = NULL;
   timer = NULL;

   if (!al_init())
      nev_abort_game("Failed to initialize allegro");
   if (debugging_is_on) nev_printdbg("al_init() success");

   if (!al_install_keyboard())
      nev_abort_game("Failed to install keyboard");
   if (debugging_is_on) nev_printdbg("al_install_keyboard() success");

   if(!al_install_mouse())
      nev_abort_game("Failed to install mouse");
   if (debugging_is_on) nev_printdbg("al_install_mouse() success");

   if (!al_init_image_addon())
      nev_abort_game("Failed to initialize image addon");
   if (debugging_is_on) nev_printdbg("al_init_image_addon() success");

   timer = al_create_timer(1.0 / FPS);
   if (!timer)
      nev_abort_game("Failed to create timer");
   if (debugging_is_on) nev_printdbg("al_create_timer() success");

   nev_ico = al_load_bitmap(IconPath(NEV_ICON));
   if (!nev_ico)
      nev_abort_game("Failed to load " IconPath(NEV_ICON));
   if (debugging_is_on) nev_printdbg(IconPath(NEV_ICON) " was loaded");

   nev_hurt_ico = al_load_bitmap(IconPath(NEV_HURT_ICON));
   if (!nev_hurt_ico)
      nev_abort_game("Failed to load " IconPath(NEV_HURT_ICON));
   if (debugging_is_on) nev_printdbg(IconPath(NEV_HURT_ICON) " was loaded");

   al_set_new_display_flags(ALLEGRO_WINDOWED);
   nev_display = al_create_display(window_width, window_height);
   if (!nev_display)
      nev_abort_game("Failed to create nev_display");
   if (debugging_is_on) nev_printdbg("al_create_display() success");

   al_set_display_icon(nev_display, nev_ico);
   if (debugging_is_on) nev_printdbg("Window icon is set to " IconPath(NEV_ICON));

   al_set_window_title(nev_display, window_title);
   if (debugging_is_on) nev_printfdbg("Window title is set to %s", window_title);

   al_init_font_addon();
   if (debugging_is_on) nev_printdbg("al_init_font_addon() success");

   if (!al_init_ttf_addon())
      nev_abort_game("Failed to initialize TTF add-on");
   if (debugging_is_on) nev_printdbg("al_init_ttf_addon() success");

   DroidSansMono_12 = al_load_font(FontPath(DROIDSANSMONO), 12, 0);
   if (!DroidSansMono_12)
      nev_abort_game("Failed to load " FontPath(DROIDSANSMONO));
   if (debugging_is_on) nev_printdbg(FontPath(DROIDSANSMONO) " was loaded");

   lunchds_small = al_load_font(FontPath(LUNCHDS), window_height/10, 0);
   if (!lunchds_small)
      nev_abort_game("Failed to load " FontPath(LUNCHDS));
   if (debugging_is_on) nev_printdbg(FontPath(LUNCHDS) " was loaded");

   lunchds_big = al_load_font(FontPath(LUNCHDS), window_height/2.5, 0);
   if (!lunchds_big)
      nev_abort_game("Failed to load " FontPath(LUNCHDS));
   if (debugging_is_on) nev_printdbg(FontPath(LUNCHDS) " was loaded");

   event_queue = al_create_event_queue();
   if (!event_queue)
      nev_abort_game("Failed to create event queue");
   if (debugging_is_on) nev_printdbg("al_create_event_queue() success");

   if (!xc_install())
      nev_abort_game("Failed to install controllers");
   if (debugging_is_on) nev_printdbg("xc_install() success");

   if (!(x360controller = xc_get_state(0))) {
      if (debugging_is_on) nev_printdbg("No Xbox controller detected");
   } else {
      if (debugging_is_on) nev_printdbg("Xbox controller detected");
   }

   al_register_event_source(event_queue, al_get_display_event_source(nev_display));
   al_register_event_source(event_queue, al_get_timer_event_source(timer));
   al_register_event_source(event_queue, al_get_keyboard_event_source());
   al_register_event_source(event_queue, al_get_mouse_event_source());
   if (x360controller)
      al_register_event_source(event_queue, xc_get_event_source());

   al_set_new_bitmap_flags(ALLEGRO_VIDEO_BITMAP);

   al_set_new_display_flags(ALLEGRO_OPENGL_3_0);

   nev_running = true;
}

/*
 * Cleanup function
 */
void nev_Shutdown(void)
{
   if (debugging_is_on) {
      nev_printdbg("");
      nev_printdbg("");
      nev_printdbg("\tnev_Shutdown");
   }

   if (nev_display)
      al_destroy_display(nev_display);
   if (debugging_is_on) nev_printdbg("nev_display is freed");

   al_destroy_font(DroidSansMono_12);
   if (debugging_is_on) nev_printdbg("DroidSansMono_12 is freed");

   al_destroy_font(lunchds_small);
   if (debugging_is_on) nev_printdbg("lunchds_small is freed");

   al_destroy_font(lunchds_big);
   if (debugging_is_on) nev_printdbg("lunchds_big is freed");

   al_destroy_bitmap(nev_ico);
   if (debugging_is_on) nev_printdbg("nev_ico is freed");

   al_destroy_bitmap(nev_hurt_ico);
   if (debugging_is_on) nev_printdbg("nev_hurt_ico is freed");

   if (event_queue)
      al_destroy_event_queue(event_queue);
   if (debugging_is_on) nev_printdbg("event_queue is freed");
   al_destroy_timer(timer);
   if (debugging_is_on) nev_printdbg("timer is freed");

   al_uninstall_system();       //only if atexit is not registered
   if (debugging_is_on) nev_printdbg("al_uninstall_system() success");
}

/*
 * Functions that are called when the screen needs to be updated
 */
void nev_update(int flag)
{
   nev_setPlayerSpeed(main_player, flag);
   nev_movePlayerEyes(main_player, flag);
   nev_movePlayer(main_player, flag);
   nev_updateBackground(test_stage, main_player, flag);
}

/*
 * Temporary keyboard message handler function for in-game messages
 */
inline void game_keyboard_events(ALLEGRO_EVENT ev, bool *redraw)
{
   int end_flag = RESET;

   switch (ev.type) {
   case ALLEGRO_EVENT_TIMER:
      if (nev_keyboard[KEY_UP] && !nev_keyboard[KEY_DOWN]) {
         end_flag = KEY_UP;
         if (main_player->pltfrm_idx >= 0) {
            nev_movePlayerEyes(main_player, end_flag);
            nev_resetPlayer(main_player);
         }
      }
      if (nev_keyboard[KEY_DOWN] && !nev_keyboard[KEY_UP]) {
         end_flag = KEY_DOWN;
         if (main_player->pltfrm_idx >= 0) {
            nev_movePlayerEyes(main_player, end_flag);
            nev_resetPlayer(main_player);
         }
      }
      if (nev_keyboard[KEY_LEFT] && !nev_keyboard[KEY_RIGHT]) {
         end_flag = KEY_LEFT;
         nev_update(end_flag);
      }
      if (nev_keyboard[KEY_RIGHT] && !nev_keyboard[KEY_LEFT]) {
         end_flag = KEY_RIGHT;
         nev_update(end_flag);
      }
      if (nev_keyboard[KEY_Z]) {
         if (nev_keyboard[KEY_DOWN]) {
            if (!pr_message_z) {
               if (main_player->pltfrm_idx >= 0) {
                  test_stage->platforms[main_player->pltfrm_idx]->pltf_gravity = test_stage->gravity = false;
               }
            }
         } else {
            if (main_player->pltfrm_idx != -1 && !main_player->jumps) {
               main_player->jumping = true;
            }
            nev_enableStageGravity(test_stage);
            pr_message_z = true;
         }
      }
      if (nev_keyboard[KEY_X]) {
         pr_message_x = true;
      }
      /*if (nev_keyboard[]) {
      }*/
      *redraw = true;
      break;
   case ALLEGRO_EVENT_KEY_UP:
      switch (ev.keyboard.keycode) {
      case ALLEGRO_KEY_UP:
         nev_setPlayerSpeed(main_player, RESET|KEY_UP);
         nev_movePlayerEyes(main_player, RESET);
         nev_keyboard[KEY_UP] = false;
         break;
      case ALLEGRO_KEY_DOWN:
         nev_setPlayerSpeed(main_player, RESET|KEY_UP);
         nev_movePlayerEyes(main_player, RESET);
         nev_keyboard[KEY_DOWN] = false;
         break;
      case ALLEGRO_KEY_RIGHT:
         nev_setPlayerSpeed(main_player, RESET|KEY_LEFT);
         nev_movePlayerEyes(main_player, RESET);
         nev_keyboard[KEY_RIGHT] = false;
         break;
      case ALLEGRO_KEY_LEFT:
         nev_setPlayerSpeed(main_player, RESET|KEY_LEFT);
         nev_movePlayerEyes(main_player, RESET);
         nev_keyboard[KEY_LEFT] = false;
         break;
      case ALLEGRO_KEY_Z:
         main_player->jumping = false;
         main_player->jumps = 0;
         nev_keyboard[KEY_Z] = false;
         break;
      case ALLEGRO_KEY_X:
         nev_keyboard[KEY_X] = false;
         break;
      case ALLEGRO_KEY_PGUP:
         if (debugging_is_on) {
            _dbg_r = 255 - _dbg_r;
            _dbg_g = 255 - _dbg_g;
            _dbg_b = 255 - _dbg_b;
         }
         break;
      case ALLEGRO_KEY_PGDN:
         if (debugging_is_on) nev_printdbg("Debugging messages disabled");
         else nev_printdbg("Debugging messages enabled");
         debugging_is_on = !debugging_is_on;
         break;
      case ALLEGRO_KEY_ESCAPE:
         game_running = false;
         break;
      }
      break;
   case ALLEGRO_EVENT_KEY_DOWN:
      switch (ev.keyboard.keycode) {
      case ALLEGRO_KEY_UP:
         nev_keyboard[KEY_UP] = true;
         break;
      case ALLEGRO_KEY_DOWN:
         nev_keyboard[KEY_DOWN] = true;
         break;
      case ALLEGRO_KEY_RIGHT:
         nev_keyboard[KEY_RIGHT] = true;
         break;
      case ALLEGRO_KEY_LEFT:
         nev_keyboard[KEY_LEFT] = true;
         break;
      case ALLEGRO_KEY_Z:
         nev_keyboard[KEY_Z] = true;
         break;
      case ALLEGRO_KEY_X:
         nev_keyboard[KEY_X] = true;
         break;
      }
      break;
   case ALLEGRO_EVENT_DISPLAY_CLOSE:
      nev_running = game_running = false;
      break;
   }
   /*temporary hack to avoid weird key combinations being pressed and the eyes not resetting
   main_player->moving = nev_keyboard[KEY_DOWN]|nev_keyboard[KEY_UP];
   main_player->moving = main_player->moving|nev_keyboard[KEY_LEFT]|nev_keyboard[KEY_RIGHT];*/
}

/*
 * Dbg: Prints values on the screen
 */
inline void nev_draw_debugmsg(void)
{
   /*** on-screen debugging messages go here, space between each message is 13 px ***/
   al_draw_text(DroidSansMono_12, al_map_rgb(0, 255, 0), 10, 0, 0, "Debug log (PAGE_UP->color/PAGE_DOWN->disable)");
   __DBG_MSG  17, 0, "lefteye_den:  .x="FRMT_FLOAT", .y="FRMT_FLOAT, main_player->lefteye_den.x, main_player->lefteye_den.y);
   __DBG_MSG  30, 0, "righteye_den:  .x="FRMT_FLOAT", .y="FRMT_FLOAT, main_player->righteye_den.x, main_player->righteye_den.y);
   __DBG_MSG  43, 0, "head_pos:  .x="FRMT_FLOAT" .y="FRMT_FLOAT, main_player->head_pos.x, main_player->head_pos.y);
   __DBG_MSG  56, 0, "body_pos:  .x="FRMT_FLOAT", .y="FRMT_FLOAT, main_player->body_pos.x, main_player->body_pos.y);
   __DBG_MSG  69, 0, "velocity:  dx="FRMT_FLOAT", dy="FRMT_FLOAT, main_player->d.x, main_player->d.y);
   __DBG_MSG  82, 0, "rightbndry:  "FRMT_FLOAT, main_player->rightbndry);
   __DBG_MSG  95, 0, "leftbndry:   "FRMT_FLOAT, main_player->leftbndry);
   __DBG_MSG 108, 0, "downbndry:   "FRMT_FLOAT, main_player->downbndry);
   __DBG_MSG 121, 0, "upbndry:  "FRMT_FLOAT, main_player->upbndry);
   __DBG_MSG 134, 0, "total_platforms:  %u", test_stage->total_platforms);
   int i, j;
   for (i = 0, j = 147; i < test_stage->total_platforms; i++, j+=13) {
      __DBG_MSG j, 0, "platform #%d:  .x="FRMT_FLOAT", .y="FRMT_FLOAT, i,
                  test_stage->platforms[i]->pltf_upper.x, test_stage->platforms[i]->pltf_upper.y);
   }
   if (main_player->jumping) {
      __DBG_MSG j+=13, 0, "main_player is jumping");
   } else {
      __DBG_MSG j+=13, 0, "main_player is not jumping");
   }
   if (main_player->pltfrm_idx >= 0) {
      __DBG_MSG j+=13, 0, "max_jump_height is "FRMT_FLOAT , main_player->max_jump_height);
      __DBG_MSG j+=13, 0, "main_player on top of platform %d", main_player->pltfrm_idx);
   }
}

/*
 * All the game drawing and logic goes here
 */
void game_loop(void)
{
   if (!game_running) return;

   if (debugging_is_on) nev_printdbg("\tgame_loop");
   ResetBoolArray(nev_keyboard, TOTAL_KEYS);

   al_unregister_event_source(event_queue, al_get_mouse_event_source());
   test_stage = nev_newBackground(StagePath(REDSUNSETPALM), RANDOM_PLATFORMS);
   if (!test_stage) nev_abort_game("Failed initializing test_stage");
   if (debugging_is_on) nev_printdbg("test_stage was created");

   main_player = nev_newPlayer(50.0f, 50.0f);
   if (!main_player) nev_abort_game("Failed initializing main_player");
   if (debugging_is_on) nev_printdbg("main_player was created");

   nev_enableStageGravity(test_stage);
   bool redraw = false;

   while (game_running) {
      ALLEGRO_EVENT event;
      al_wait_for_event(event_queue, &event);

      game_keyboard_events(event, &redraw);

      if (pr_message_z) {
         pr_message_z = false;
         if (!main_player->jumps)
            nev_playerJump(main_player, test_stage);
      }
      if (!main_player->jumping)
         nev_setPlatformCollision(main_player, test_stage);

      if (redraw && al_is_event_queue_empty(event_queue)) {
         if (main_player->body_pos.y >= window_height - 90)
            game_running = false;

         redraw = false;
         al_clear_to_color(NEV_BLACK);
         nev_drawBackground(test_stage);

         nev_drawPlayer(main_player);
         if (debugging_is_on) {
            nev_draw_debugmsg();
         }

         if (pr_message_x) {
            pr_message_x = false;
            al_draw_text(lunchds_small, NEV_CYAN, 300, 250, 0, "X key not yet programmed ;)");
         }
         al_flip_display();
      }
   }
   al_flush_event_queue(event_queue);
   nev_destroyPlayer(main_player);
   nev_destroyBackground(test_stage);
}

/*
 * Runs the game loops after all initializations are complete
 */
void run_game(void)
{
   nev_init();

   al_set_target_backbuffer(nev_display);
   al_start_timer(timer);

   while (nev_running) {
      menu_running = true;
      main_menu_loop();
      game_loop();
   }

   nev_Shutdown();
}

/*
 * Main entry point
 */
int main(int argc, char *argv[])
{
   int opt;

   debugging_is_on = false;
   while ((opt = getopt(argc, argv, "dv")) != -1) {
      switch (opt) {
         case 'd':
            debugging_is_on = true;
            break;
         case 'v':
            fprintf(stdout, "Nev "NEV_VERSION_STRING"\n"
                    "Copyright (C) 2015 George Koskeridis, see LICENSE");
            return 1;
         default:
            break;
      }
   }
   if (debugging_is_on) nev_printdbg("Nev "NEV_VERSION_STRING" debugging console");

   window_width = 1000;
   window_height = 400;
   run_game();

	return 0;
}
